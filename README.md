# Projeto02 - Licença Ambiental


### Descrição do Projeto

**Responsável**: Maiel

Sistema para uma empresa ambiental que tem necessidade de guardar e avisar a validades das licenças de seus cliente , deve fazer o alerta 15 dias antes de cada licença perder a validade, e fazer o alerta no dia do vencimento, cada cliente pode e terá mais que uma licença cadastrada, os alertas deverão ser apresentados após o login no sistema diariamente.


### Modelagem

**Responsável**: Sr. Khassim Mbaye

Requisitos:
O sistema deverá manter os dados dos clientes;
O sistema deverá manter as licenças dos clientes;
O sistema deverá notificar os usuários após o login sobre as licenças que estão prestes a perder a validade nos intervalos de 15 dias antes do vencimento e no dia do vencimento.

Entidades:
Cliente
CPF/CNPJ
Nome
Contato
Endereço

Licença
Cliente
Nro da Licença
Tipo licença
Data de Emissão
Data de Validade



### Prototipagem

**Responsável**: Serigne Khassim


